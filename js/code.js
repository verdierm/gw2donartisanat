var lang = 'fr';
var sortOrder = 'sell';
var items = [];
var tpItems = [];
var prices = [];
var recipes = [];
var recipesID = [];

var itemsID = [
	74356,										// Natomi: Tab 1: Collectionneur
	46281, 46186, 46040, 45622, 45765, 45731,	// Tab 2: Sylvari
	15465, 14469, 13924, 10702, 11876, 11121,	// Tab 3: Itzel
	15394, 14517, 13895, 10722, 11798, 11295,	// Tab 4: Pacte
	15508, 14596, 13974, 10710, 11835, 11248,	// Tab 5: Noble
	36779, 36750, 36813, 36746, 36891, 36892,	// Tab 6: Quaggan
	73537,										// Rakatin: Tab 1: Collectionneur
	38336, 38367, 38415, 38179, 38264, 38228,	// Tab 2: Prieuré
	15352, 14566, 13895, 10722, 11798, 11295,	// Tab 3: Exalté
	15427, 14648, 13928, 10699, 11754, 11167,	// Tab 4: Skritt
	72205,										// Ravitaillement: Tab 1: Collectionneur
	15391, 14563, 13976, 10691, 11921, 11341,	// Tab 2: Ogre
	15423, 14428, 13973, 10709, 11834, 11247,	// Tab 3: Rata Novus
	15512, 14516, 13894, 10707, 11881, 11126,	// Tab 4: Nuhoch
	36779, 36812, 36780, 36806, 36842, 36844,	// Tab 5: B.R.U.T.A.L
	19983, 19721, 24830,						// Arche du lion
	43772, 24330, 24726,						// Rata Sum
	46744, 66650, 24735,						// Le Bosquet
	46742, 24678, 24732,						// Promontoir Divin
	46745, 24651, 24729,						// Hoelbrak
	19925, 24366, 24741							// Citadelle Noire
];

gw2bltcUrl = 'https://www.gw2bltc.com/' + lang + '/item/';
poIcon = '<img class="coin" src="https://wiki.guildwars2.com/images/d/d7/Gold_coin_%28highres%29.png">'
paIcon = '<img class="coin" src="https://wiki.guildwars2.com/images/c/c1/Silver_coin_%28highres%29.png">'
pcIcon = '<img class="coin" src="https://wiki.guildwars2.com/images/1/11/Copper_coin_%28highres%29.png">'
var addBtn = '<i class="material-icons">arrow_drop_down</i>'
var minBtn = '<i class="material-icons">arrow_drop_up</i>'

$(document).ready(function() {
	var recipesIdUrl = 'https://api.guildwars2.com/v2/recipes';

	$('.tpLink').click(copyLink)
	if (!localStorage || !localStorage.getItem('recipes')) {
		console.log('No recipes in localStorage');
		$.ajax({
			url: recipesIdUrl,
			async: false
		}).done(function(data) {
			recipesID = data;
			$.when(getAllRecipes(0)).then(function() {
				recipes.push({
					'ingredients': [{
						'count': 25,
						'item_id': 43773
					}],
					'output_item_id': 43772,
					'output_item_count': 1
				});
				try {
					localStorage.setItem('recipes', JSON.stringify(recipes));
					console.log('Saved recipes in localStorage');
				} catch(error) {
					console.error(error);
					console.log('Cannot save recipes on localStorage');
				}
				prepareDisplay();
			});
		});
	}
	else {
		recipes = JSON.parse(localStorage.getItem('recipes'));
		console.log('Retrieved recipes from localStorage: ' + recipes.length);
		prepareDisplay();
	}
});

function prepareDisplay() {
	var ingredientsIds = getIngredientsIds(itemsID);
	ingredientsIds = ingredientsIds.filter((item, index) => ingredientsIds.indexOf(item) === index);
	itemsID = itemsID.concat(ingredientsIds);
	$.when(getItems(0)).then(function() {
		$.when(getPrices(itemsID, 0)).then(function() {
			displayMaps();
			$('button[name="sell"]').click(function() {
				changeSortOrder('sell');
			});
			$('button[name="buy"]').click(function() {
				changeSortOrder('buy');
			});
			$('button[name="reset-cache"]').click(function() {
				if (localStorage && localStorage.getItem('recipes'))
					localStorage.clear();
			});
			prices.forEach((price, id) => {
				prices[id]['craft'] = getCraftPrice(id);
			});
			$('button[name="craft"]').click(function() {
				changeSortOrder('craft');
			});
		});
	});
}

function getItems(i) {
	var defer = $.Deferred();

	if (i < itemsID.length) {
		var itemsUrl = 'https://api.guildwars2.com/v2/items?lang=' + lang + '&ids=' + itemsID.slice(i, i + 200);
		$.ajax({
			url: itemsUrl
		}).done(function(data) {
			items = items.concat(data);
			$.when(getItems(i + 200)).then(function() {
				defer.resolve();
			});
		});
	}
	else
		defer.resolve();
	return defer;
}

function changeSortOrder(order) {
	sortOrder = order;
	displayMaps();
}

function getAllRecipes(i) {
	var defer = $.Deferred();

	if (i < recipesID.length) {
		var recipesUrl = 'https://api.guildwars2.com/v2/recipes?lang=' + lang + '&ids=' + recipesID.slice(i, i + 200);
		$.ajax({
			url: recipesUrl
		}).done(function(data) {
			recipes = recipes.concat(data);
			$.when(getAllRecipes(i + 200)).then(function() {
				defer.resolve();
			});
		});
	}
	else
		defer.resolve();
	return defer;
}

function getPrices(ids, i) {
	var defer = $.Deferred();

	if (i < ids.length) {
		var tpItemsUrl = 'https://api.guildwars2.com/v2/commerce/prices?lang=' + lang + '&ids=' + ids.slice(i, i + 200);
		$.ajax({
			url: tpItemsUrl
		}).done(function(data) {
			tpItems = tpItems.concat(data);
			ids.forEach(id => {
				var tpItem = tpItems[tpItems.findIndex(elem => elem.id === id)];
				prices[id] = {
					'sell': tpItem ? tpItem.sells.unit_price : 0,
					'buy': tpItem ? tpItem.buys.unit_price : 0
				};
			});
			getPrices(ids, i + 200).then(function() {
				defer.resolve();
			});
		});
	}
	else
		defer.resolve();
	return defer;
}

function getCraftPrice(id) {
	var item = tpItems[tpItems.findIndex(elem => elem.id === id)];
	var price = item ? item.sells.unit_price : 0;
	var craftPrice = 0;
	var finalPrice = 0;
	var recipe = recipes[recipes.findIndex(elem => elem.output_item_id === id)];

	if (recipe) {
		recipe.ingredients.forEach(ingredient => {
			var ingredientTpItem = tpItems[tpItems.findIndex(elem => elem.id === ingredient.item_id)];
			var ingredientPrice = ingredient.count * (ingredientTpItem ? ingredientTpItem.sells.unit_price : 0);
			var ingredientCraftPrice = ingredient.count * getCraftPrice(ingredient.item_id);
			craftPrice += (((ingredientPrice > 0 && ingredientPrice) <= ingredientCraftPrice
				|| ingredientCraftPrice === 0 || ingredientCraftPrice === 999999999) ? ingredientPrice : ingredientCraftPrice);
		});
	}
	else
		craftPrice = 999999999;

	if (craftPrice < 999999999 && recipe)
		craftPrice = Math.floor(craftPrice / recipe.output_item_count);
	finalPrice = ((price > 0 && price <= craftPrice)
		|| craftPrice === 0 || craftPrice >= 999999999) ? price : craftPrice;
	return (finalPrice);
}

function getIngredientsIds(ids) {
	var ingredientsIds = [];

	ids.forEach(id => {
		var recipe = recipes[recipes.findIndex(elem => elem.output_item_id === id)];
		if (recipe) {
			recipe.ingredients.forEach(ingredient => {
				ingredientsIds.push(ingredient.item_id);
			});
		}
	});

	if (ingredientsIds.length > 0)
		ingredientsIds = ingredientsIds.concat(getIngredientsIds(ingredientsIds));
	return ingredientsIds;
}

function displayMaps() {
	displayVb();
	displayAb();
	displayTd();
	displayArche();
	displayRata();
	displayBosquet();
	displayPromontoir();
	displayHoelbrak();
	displayCitadelle();
}

function displayVb() {
	vb = [
		[74356],
		[46281, 46186, 46040, 45622, 45765, 45731],
		[15465, 14469, 13924, 10702, 11876, 11121],
		[15394, 14517, 13895, 10722, 11798, 11295],
		[15508, 14596, 13974, 10710, 11835, 11248],
		[36779, 36750, 36813, 36746, 36891, 36892]
	];

	displayLowerPricePerTab(vb, '#vb');
}

function displayAb() {
	ab = [
		[73537],
		[38336, 38367, 38415, 38179, 38264, 38228],
		[15352, 14566, 13895, 10722, 11798, 11295],
		[15427, 14648, 13928, 10699, 11754, 11167]
	];

	displayLowerPricePerTab(ab, '#ab');
}

function displayTd() {
	td = [
		[72205],
		[15391, 14563, 13976, 10691, 11921, 11341],
		[15423, 14428, 13973, 10709, 11834, 11247],
		[15512, 14516, 13894, 10707, 11881, 11126],
		[36779, 36812, 36780, 36806, 36842, 36844]
	];

	displayLowerPricePerTab(td, '#td');
}

function displayArche() {
	arche = [
		[19983, 1],
		[19721, 5],
		[24830, 1]
	];

	displayPricePerTab(arche, '#arche');
}

function displayRata() {
	rata = [
		[43772, 1],
		[24330, 24],
		[24726, 14]
	];

	displayPricePerTab(rata, '#rata');
}

function displayBosquet() {
	bosquet = [
		[46744, 1],
		[66650, 3],
		[24735, 14]
	];

	displayPricePerTab(bosquet, '#bosquet');
}

function displayPromontoir() {
	promontoir = [
		[46742, 1],
		[24678, 34],
		[24732, 4]
	];

	displayPricePerTab(promontoir, '#promontoir');
}

function displayHoelbrak() {
	hoelbrak = [
		[46745, 1],
		[24651, 20],
		[24729, 14]
	];

	displayPricePerTab(hoelbrak, '#hoelbrak');
}

function displayCitadelle() {
	citadelle = [
		[19925, 1],
		[24366, 20],
		[24741, 12]
	];

	displayPricePerTab(citadelle, '#citadelle');
}

function displayLowerPricePerTab(map, tag) {
	map.forEach(element => {
		tag = $(tag).next();
		element.sort(sortItemsByPrices);

		$(tag).children().last().children().last().html('');
		element.forEach((id, i) => {
			var item = items[items.findIndex(elem => elem.id === id)];

			var pc = parseInt(prices[id][sortOrder] % 100);
			var pa = parseInt((prices[id][sortOrder] % 10000) / 100);
			var po = parseInt(prices[id][sortOrder] / 10000);
			var icon = $(document.createElement('img')).attr({
				src: item.icon,
				class: 'icon'
			});
			var link = $(document.createElement('a')).attr({
				href: gw2bltcUrl + id,
				target: '_blank'
			}).html(item.name);
			var div = $(document.createElement('div')).attr({
			});
			div.append(icon);
			div.append(link);
			if (prices[item.id][sortOrder] > 0) {
				div.html(div.html() + ': ' + (sortOrder === 'craft' ? '~' : '')
					+ (po > 0 ? po + poIcon : '')
					+ (pa > 0 ? (pa < 10 ? '0' + pa : pa ) + paIcon : '')
					+ (pc < 10 ? '0' + pc : pc) + pcIcon);
			}
			var li = $(document.createElement('li'))
			if (i > 0)
				li.attr({class: 'extendItem'});
			li.append(div);
			$(tag).children().last().children().last().append(li);
		});

		var extend = $(document.createElement('div')).attr({
			class: 'extendItemList'
		}).html(addBtn);

		extend.click(extendItemList);
		var first = $(tag).children().last().children().first();
		if (element.length > 1 && first.attr('class') !== 'extendItemList')
			$(tag).children().last().prepend(extend);
		else if (element.length > 1)
			first.html(addBtn);
	});
}

function displayPricePerTab(map, tag) {
	tag = $(tag).next();

	$(tag).children().last().children().last().html('');
	map.forEach(element => {
		var item = items[items.findIndex(elem => elem.id === element[0])];

		var pc = parseInt((element[1] * prices[item.id][sortOrder]) % 100);
		var pa = parseInt(((element[1] * prices[item.id][sortOrder]) % 10000) / 100);
		var po = parseInt((element[1] * prices[item.id][sortOrder]) / 10000);

		var icon = $(document.createElement('img')).attr({
			src: item.icon,
			class: 'icon'
		});

		var link = $(document.createElement('a')).attr({
			href: gw2bltcUrl + element[0],
			target: '_blank'
		}).html(item.name);

		var div = $(document.createElement('div')).attr({
		});

		div.html(div.html() + element[1] + ' ');
		div.append(icon);
		div.append(link);

		if (prices[item.id][sortOrder] > 0) {
			div.html(div.html() + ': ' + (sortOrder === 'craft' ? '~' : '')
				+ (po > 0 ? po + poIcon : '')
				+ (pa > 0 ? (pa < 10 ? '0' + pa : pa ) + paIcon : '')
				+ (pc < 10 ? '0' + pc : pc) + pcIcon);
		}

		var li = $(document.createElement('li')).append(div);
		$(tag).children().last().children().last().append(li);
	});
}

function sortItemsByPrices(a, b) {
	return (prices[a][sortOrder] > prices[b][sortOrder]);
}

function extendItemList() {
	var list = $(this).next().children();

	this.innerHTML = this.innerHTML === addBtn ? minBtn : addBtn;
	list.each((index, element) => {
		if (index === 0)
			return ;
		var display = $(element).attr('class');
		if (display === 'extendItem')
			$(element).removeAttr('class');
		else
			$(element).attr({class: 'extendItem'});
	});
}

function fallbackCopyTextToClipboard(text) {
	var textArea = document.createElement("textarea");
	textArea.value = text;
	textArea.style.position="fixed";
	document.body.appendChild(textArea);
	textArea.focus();
	textArea.select();

	try {
	var successful = document.execCommand('copy');
	var msg = successful ? 'successful' : 'unsuccessful';
	console.log('Fallback: Copying text command was ' + msg);
	} catch (err) {
		console.error('Fallback: Oops, unable to copy', err);
	}

	document.body.removeChild(textArea);
}

function copyLink(text) {
	if (!navigator.clipboard) {
		fallbackCopyTextToClipboard(this.innerText);
		return;
	}
	navigator.clipboard.writeText(this.innerText).then(function() {
		console.log('Async: Copying to clipboard was successful!');
	}, function(err) {
		console.error('Async: Could not copy text: ', err);
	});
}
